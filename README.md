#### AWS Isolation explained
3 ways.
1 is based on ARN, this is hard to maintain so good luck maintaining this.
2 is based on tag, which works well for ec2 for example.
3 is use resource policies

1 & 2 are json statements in IAM policies. If your IAM is centralized this is easy, if teams can make their own roles, let them make the IAM resources using service-catalog so you can enforce a product specification or force attach this isolation policy.
Anyways, the JSON is explained below. 

### The first: resource ARN
##### The DenyNotResourceList
`{
    "Sid": "DenyNotResourceListIAM",
    "Effect": "Deny",
    "Action": [
        "iam:bla*"
    ],
    "NotResource": [
        "arn:aws:iam::131313131313:role/t-team*",
        "arn:aws:iam::131313131313:policy/t-team*"
    ]
},`

This denies the action iam:bla on all resources that are OUTSIDE the t-team* name path in account 131313131313. The NotResource parameter will match the resources specified, then apply the Deny to all other resources.

### The second: resource tag
##### The DenyResourceListConditionalTagExists

`        {
            "Sid": "DenyResourceListConditionalTagExists",
            "Effect": "Deny",
            "Action": [
                "ec2:bla"
            ],
            "Resource": [
                "arn:aws:ec2:eu-west-1:131313131313:instance/*",
                "arn:aws:ec2:eu-west-1:131313131313:security-group/*",
                "arn:aws:ec2:eu-west-1:131313131313:etc..."
            ],
            "Condition": {
                "Null": {
                    "ec2:ResourceTag/team": "false"
                },
                "StringNotEqualsIfExists": {
                    "ec2:ResourceTag/team": "awsb"
                }
            }
        },`

This denies all actions on resources that are tagged with the `team` tag and where the `team` is not set to the team `awsb`. The condition checks if the team tag is present. If it is present AND it is NOT `awsb`, the action ec2:bla is denied.

##### The DenyOtherValueForTeamTag

`{
    "Sid": "DenyOtherValueForTeamTag",
    "Effect": "Deny",
    "Action": "ec2:CreateTags",
    "Resource": [
        "arn:aws:ec2:eu-west-1:131313131313:instance/*",
        "arn:aws:ec2:eu-west-1:131313131313:etc..."
    ],
    "Condition": {
        "StringNotEquals": {
            "aws:RequestTag/team": "example"
        },
        "ForAllValues:StringEquals": {
            "aws:TagKeys": [
                "team"
            ]
        }
    }`

This statement denies via an IAM set operation all values on the key `team` unless the value is `example`. As you can see, this is done on the RequestTag instead of the ResourceTag. All other tag values are allowed, as the Set operation is placed in an AND operation together with the condition on the RequestTag.


### The third: resource policies
Resource policies enable teams to create their own access permissions on a resource, for example a S3 bucket or an SNS topic. They can specify who can modify what on that resource. This would be a teams own responsibility.